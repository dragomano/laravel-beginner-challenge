<?php

namespace Database\Seeders;

use App\Models\{Article, Category, Tag, User};
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin@test.com')
        ]);

        $categories = Category::factory(5)->create();

        $sequence = new Sequence(
            fn () => ['category_id' => $categories->random()->id]
        );

        $articles = Article::factory(30)->state($sequence)->randomDate()->create();

        $tags = Tag::factory(20)->create();

        $articles->each(function ($article) use ($tags) {
            $article->tags()->attach(($tags->random(rand(0, 4))));
        });
    }
}
