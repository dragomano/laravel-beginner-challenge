<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image = 'https://picsum.photos/seed/' . Str::random() . '/400/200';

        return [
            'category_id' => function() {
                return Category::factory()->create()->id;
            },
            'title' => Str::ucfirst($this->faker->unique()->words(rand(3, 10), true)),
            'content' => implode(PHP_EOL, $this->faker->paragraphs(rand(1, 6))),
            'image' => $this->faker->randomElement([null, $image]),
        ];
    }

    /**
     * Indicate that the article dates should be random
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function randomDate()
    {
        return $this->state(function (array $attributes) {
            return [
                'created_at' => $date = $this->faker->dateTimeBetween('-30 days'),
                'updated_at' => $date,
            ];
        });
    }
}
