<x-app-layout>
    <div class="w-full px-4 md:px-6 text-xl text-gray-800 leading-normal" style="font-family:Georgia,serif;">
        <div class="font-sans">
            <p class="text-base md:text-sm text-green-500 font-bold">&lt; <a href="{{ url('/') }}" class="text-base md:text-sm text-green-500 font-bold no-underline hover:underline">
                    {{ __('BACK TO BLOG') }}
                </a>
            </p>
            <h1 class="font-bold font-sans break-normal text-gray-900 pt-6 pb-2 text-3xl md:text-4xl">{{ $article->title }}</h1>
            <p class="float-left text-sm md:text-base font-normal text-gray-600">{{ $article->created_at->diffForHumans() }}</p>
            <p class="float-right">
                <a class="text-base md:text-sm text-green-500 font-bold no-underline hover:underline" href="{{ route('categories.show', $article->category) }}">
                    {{ $article->category->name }}
                </a>
            </p>
            <img src="{{ $article->getImageUrl() }}" class="w-full object-cover lg:rounded" style="height: 28em;"/>
        </div>

        <div class="py-6">
            {!! $article->content !!}
        </div>
    </div>

    <x-tags :tags="$article->tags" />
    <x-divider />
    <x-subscribe />
    <x-article-author />
    <x-divider />
    <x-next-prev :prevArticle="$prevArticle" :nextArticle="$nextArticle" />
</x-app-layout>
