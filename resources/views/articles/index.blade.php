<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Article list') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="mx-auto sm:px-6 lg:px-8">
            <div class="flex flex-wrap">

                @foreach ($articles as $article)
                    <x-article-card :article="$article" />
                @endforeach

            </div>

            {{ $articles->links() }}
        </div>
    </div>
</x-app-layout>
