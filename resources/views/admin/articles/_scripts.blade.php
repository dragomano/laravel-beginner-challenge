@push('css')
<link href="https://cdn.jsdelivr.net/npm/slim-select@1/dist/slimselect.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/suneditor@latest/dist/css/suneditor.min.css" rel="stylesheet">
@endpush

<script src="https://cdn.jsdelivr.net/npm/suneditor@latest/dist/suneditor.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/slim-select@1/dist/slimselect.min.js"></script>
<script>
    const sunEditor = SUNEDITOR.create('content')
    sunEditor.onChange = () => sunEditor.save()

    new SlimSelect({
        select: '#category_id'
    })

    new SlimSelect({
        select: '#tags',
        closeOnSelect: false,
        hideSelectedOption: true,
        addable: function (value) {
            return {
                text: value.toLowerCase(),
                value: value.toLowerCase()
            }
        }
    })
</script>
