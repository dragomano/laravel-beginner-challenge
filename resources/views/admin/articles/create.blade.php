<x-admin-layout>
    <x-slot name="header">
        {{ __('New article') }}
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />

                    <form method="POST" action="{{ route('articles.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div>
                            <x-label for="title" :value="__('Article title')" />
                            <x-input id="title" class="block mt-1 w-full" type="text" name="title" :value="old('title')" required autofocus />
                        </div>

                        <div class="mt-4">
                            <x-label for="category_id" :value="__('Category')" />
                            <select name="category_id" id="category_id">

                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"{{ old('category_id') === $category->id ? ' selected' : '' }}>{{ $category->name }}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="mt-4">
                            <x-label for="tags" :value="__('Tags')" />
                            <select name="tags[]" id="tags" multiple>

                                @foreach ($tags as $tag)
                                    <option value="{{ $tag->name }}"{{ old('tags') && in_array($tag->name, old('tags')) ? ' selected' : '' }}>{{ $tag->name }}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="mt-4">
                            <x-label for="content" :value="__('Content')" />
                            <x-textarea name="content" id="content" cols="10" rows="8" placeholder="What do you want to tell the world?">{{ old('content') }}</x-textarea>
                        </div>

                        <div class="mt-4">
                            <x-label for="image_link" :value="__('Image link')" />
                            <x-input id="image_link" class="block mt-1 w-full" type="url" name="image_link" :value="old('image_link')" placeholder="You can specify any image link or upload a desired picture from your computer" />
                        </div>

                        <div class="mt-4">
                            <div class="flex w-full items-center justify-center bg-grey-lighter">
                                <label class="w-64 flex flex-col items-center px-4 py-6 bg-white text-blue rounded-lg shadow-sm tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue hover:text-blue-400">
                                    <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z"></path>
                                    </svg>
                                    <span class="mt-2 text-base leading-normal">{{ __('Image') }}</span>
                                    <input type="file" name="image" class="hidden" accept="image/*">
                                </label>
                            </div>
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('articles.index') }}">
                                {{ __('Back to the article list') }}
                            </a>

                            <x-button class="ml-3">
                                {{ __('Create') }}
                            </x-button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    @include('admin.articles._scripts')
</x-admin-layout>
