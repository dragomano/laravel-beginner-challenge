<x-admin-layout>
    <x-slot name="header">
        {{ __('Manage articles') }}
    </x-slot>

    <div class="bg-white pb-4 px-4 rounded-md w-full">
        <div class="w-full flex justify-end px-2 mt-2">
            <div class="w-full sm:w-auto inline-block relative">
                <x-button-link :href="route('articles.create')">
                    {{ __('Add article') }}
                </x-button-link>
            </div>
        </div>
        <div class="overflow-x-auto mt-6">
            <table class="table-auto border-collapse w-full">
                <thead>
                    <tr class="rounded-lg text-sm font-medium text-gray-700 text-left" style="font-size: 0.9674rem">
                        <th class="px-4 py-2 bg-gray-200" style="background-color:#f8f8f8">Title</th>
                        <th class="px-4 py-2" style="background-color:#f8f8f8">Category</th>
                        <th class="px-4 py-2" style="background-color:#f8f8f8">Image</th>
                        <th class="px-4 py-2" style="background-color:#f8f8f8">Tags</th>
                        <th class="px-4 py-2" style="background-color:#f8f8f8">Updated at</th>
                        <th class="px-4 py-2 flex justify-end" style="background-color:#f8f8f8">Actions</th>
                    </tr>
                </thead>
                <tbody class="text-sm font-normal text-gray-700">

                @foreach ($articles as $article)
                    <tr class="hover:bg-gray-100 border-b border-gray-200 py-10">
                        <td class="px-4 py-4">
                            <a class="text-sm text-gray-600 hover:text-gray-900" href="{{ route('articles.show', $article) }}">
                                {{ $article->title }}
                            </a>
                        </td>
                        <td class="px-4 py-4">{{ $article->category->name }}</td>
                        <td class="px-4 py-4">
                            <img class="w-24" src="{{ $article->getImageUrl() }}">
                        </td>
                        <td class="px-4 py-4">
                            <span class="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                                <span aria-hidden class="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                                <span class="relative text-xs">
                                    {{ $article->tags_count }}
                                </span>
                            </span>
                        </td>
                        <td class="px-4 py-4">{{ $article->updated_at->diffForHumans() }}</td>
                        <td class="px-4 py-4 flex justify-end">
                            <a class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline" href="{{ route('articles.edit', $article) }}">
                                {{ __('Edit') }}
                            </a>
                            <form method="POST" action="{{ route('articles.destroy', $article) }}">
                                @csrf
                                @method('DELETE')

                                <button type="submit" onclick="return confirm('Sure?')" class="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">{{ __('Remove') }}</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

        {{ $articles->links() }}

    </div>
</x-admin-layout>
