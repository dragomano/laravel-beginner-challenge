<div class="bg-white my-12 pb-6 w-full justify-center items-center overflow-hidden md:max-w-sm rounded-lg shadow-sm mx-auto">
    <div class="relative h-40">
        <img class="absolute h-full w-full object-cover" src="{{ $article->getImageUrl() }}">
    </div>
    <div class="mt-6">
        <h1 class="text-lg text-center font-semibold">
            <a href="{{ route('articles.show', $article) }}">
                {{ $article->title }}
            </a>
        </h1>
        <div class="text-sm text-gray-600 text-center px-3">
            {!! $article->teaser !!}
        </div>
    </div>
    <div class="mt-6 pt-3 flex flex-wrap mx-6 border-t">

        @foreach ($article->tags as $tag)
            <div class="text-xs mr-2 my-1 uppercase tracking-wider border px-2 text-indigo-600 border-indigo-600 hover:bg-indigo-600 hover:text-indigo-100 cursor-default">
                <a href="{{ route('tags.show', $tag) }}">{{ $tag->name }}</a>
            </div>
        @endforeach

    </div>
</div>
