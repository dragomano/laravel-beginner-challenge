<!--Tags -->
@if ($tags->count())
<div class="text-base md:text-sm text-gray-500 px-4 py-6">
    Tags:
    @foreach ($tags as $tag)
        <a href="{{ route('tags.show', $tag) }}" class="text-base md:text-sm text-green-500 no-underline hover:underline">{{ $tag->name }}</a>
    @endforeach
</div>
@endif
