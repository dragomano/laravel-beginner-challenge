@props(['active'])

@php
    $classes = ($active ?? false)
                ? 'block pl-4 align-middle text-grey-darker no-underline hover:text-purple border-l-4 border-transparent lg:border-purple lg:hover:border-purple'
                : 'block pl-4 align-middle text-grey-darker no-underline hover:text-purple border-l-4 border-transparent lg:hover:border-grey-light';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    <span class="pb-1 md:pb-0 text-sm{{ $active ? ' text-black font-bold' : '' }}">
        {{ $slot }}
    </span>
</a>
