<!--Next & Prev Links-->
<div class="font-sans flex justify-between content-center px-4 pb-12">
    <div class="text-left">

        @if ($prevArticle)
            <span class="text-xs md:text-sm font-normal text-gray-600">&lt; {{ __('Previous Post') }}</span><br>
            <p><a href="{{ route('articles.show', $prevArticle) }}" class="break-normal text-base md:text-sm text-green-500 font-bold no-underline hover:underline">{{ $prevArticle->title }}</a></p>
        @endif

    </div>
    <div class="text-right">

        @if ($nextArticle)
            <span class="text-xs md:text-sm font-normal text-gray-600">{{ __('Next Post') }} &gt;</span><br>
            <p><a href="{{ route('articles.show', $nextArticle) }}" class="break-normal text-base md:text-sm text-green-500 font-bold no-underline hover:underline">{{ $nextArticle->title }}</a></p>
        @endif

    </div>
</div>
<!--/Next & Prev Links-->
