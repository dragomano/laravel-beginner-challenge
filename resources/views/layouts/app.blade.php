<!DOCTYPE html>
<html class="h-full" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body class="h-full min-h-full flex flex-col bg-gray-100 font-sans leading-normal tracking-normal">
    @include('layouts.navigation')

    <!-- Page Content -->
    <div class="container w-full flex-auto md:max-w-7xl mx-auto pt-20">
        {{ $slot }}
    </div>

    @include('layouts.footer')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
