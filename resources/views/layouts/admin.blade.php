<!DOCTYPE html>
<html class="h-full" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }} - Admin</title>
    @stack('css')
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body class="h-full min-h-full flex flex-col bg-gray-100 font-sans leading-normal tracking-normal">
    @include('layouts.navigation')

    <div class="container w-full flex flex-wrap mx-auto px-2 pt-8 lg:pt-16 mt-16">
    @include('components.admin-menu')

    <!-- Page Content -->
        <div class="w-full lg:w-4/5 p-8 mt-6 lg:mt-0 text-black leading-normal bg-white border border-grey-light border-rounded">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">{{ $header }}</h2>

            {{ $slot }}
        </div>
    </div>

    <script>
        let helpMenuDiv = document.getElementById("menu-content");
        let helpMenu = document.getElementById("menu-toggle");

        document.onclick = check;

        function check(e) {
            let target = (e && e.target) || (event && event.srcElement);

            //Help Menu
            if (! checkParent(target, helpMenuDiv)) {
                if (checkParent(target, helpMenu)) {
                    if (helpMenuDiv.classList.contains("hidden")) {
                        helpMenuDiv.classList.remove("hidden");
                    } else {helpMenuDiv.classList.add("hidden");}
                } else {
                    helpMenuDiv.classList.add("hidden");
                }
            }

        }

        function checkParent(t, elm) {
            while (t.parentNode) {
                if ( t === elm) {return true;}
                t = t.parentNode;
            }
            return false;
        }
    </script>

    <!-- Scripts -->
    @stack('js')
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
