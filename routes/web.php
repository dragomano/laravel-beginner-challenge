<?php

use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\ArticleController as HomeController;
use App\Http\Controllers\CategoryController as AllArticlesWithThisCategory;
use App\Http\Controllers\TagController as AllArticlesWithThisTag;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index']);

Route::get('articles/{article}', [HomeController::class, 'show'])
    ->whereNumber('article')
    ->name('articles.show');

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::view('/', 'admin.index')->name('admin');
    Route::resources([
        'articles' => ArticleController::class,
        'categories' => CategoryController::class,
        'tags' => TagController::class
    ], ['except' => 'show']);
});

Route::get('categories/{category}', AllArticlesWithThisCategory::class)->name('categories.show');
Route::get('tags/{tag}', AllArticlesWithThisTag::class)->name('tags.show');

Route::view('about', 'about')->name('about');

require __DIR__.'/auth.php';
