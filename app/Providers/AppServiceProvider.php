<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Tag;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::defaultView('pagination::custom');

        View::composer(['admin.articles.create', 'admin.articles.edit'], function ($view) {
            $view->with([
                'categories' => Category::all(),
                'tags' => Tag::all()
            ]);
        });
    }
}
