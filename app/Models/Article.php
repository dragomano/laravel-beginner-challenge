<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Article extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return string
     */
    public function getTeaserAttribute(): string
    {
        return Str::limit($this->content, 120);
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        if (Str::startsWith($this->image, 'http')) {
            return $this->image;
        }

        if (! $this->image) return 'https://via.placeholder.com/384x160?text=No+image';

        return asset('storage/' . $this->image);
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute(): string
    {
        return $this->isUploadedImage() ? '' : ($this->image ?? '');
    }

    /**
     * @param array $fields
     */
    public function prepareToInsert(array &$fields)
    {
        if (request()->file('image')) {
            $fields['image'] = request()->file('image')->store('images', 'public');
        } elseif ($fields['image_link']) {
            $fields['image'] = $fields['image_link'];
        }

        unset($fields['image_link']);
    }

    /**
     * @param array $fields
     * @return array
     */
    public function getPreparedTags(array &$fields): array
    {
        if (! isset($fields['tags'])) return [];

        $tags = [];
        foreach ($fields['tags'] as $tagName) {
            $tag = Tag::firstOrCreate(['name' => $tagName]);
            $tags[] = $tag->id;
        }

        unset($fields['tags']);

        return $tags;
    }

    /**
     * @param array $fields
     */
    public function prepareToUpdate(array &$fields)
    {
        if (request()->hasFile('image')) {
            $this->removeImage();

            $fields['image'] = request()->file('image')->store('images', 'public');
        } elseif ($fields['image_link']) {
            $fields['image'] = $fields['image_link'];
        }

        unset($fields['image_link']);

        $fields['updated_at'] = now();
    }

    public function removeImage()
    {
        if ($this->isUploadedImage()) {
            Storage::disk('public')->delete($this->image);
        }
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsToMany
     */
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * @return bool
     */
    private function isUploadedImage(): bool
    {
        return $this->image && Str::startsWith($this->image, 'images/');
    }
}
