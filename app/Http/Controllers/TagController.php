<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class TagController extends Controller
{
    /**
     * @param Tag $tag
     * @return Factory|View|Application
     */
    public function __invoke(Tag $tag): Factory|View|Application
    {
        $articles = $tag->articles()
            ->with('tags')
            ->latest()
            ->paginate(9);

        return view('articles.index', compact('articles'));
    }
}
