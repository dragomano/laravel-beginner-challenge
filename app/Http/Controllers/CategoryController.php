<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class CategoryController extends Controller
{
    /**
     * @param Category $category
     * @return Factory|View|Application
     */
    public function __invoke(Category $category): Factory|View|Application
    {
        $articles = $category->articles()
            ->with('tags')
            ->latest()
            ->paginate(9);

        return view('articles.index', compact('articles'));
    }
}
