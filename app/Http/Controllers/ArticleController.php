<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $articles = Article::with('tags')
            ->latest()
            ->paginate(9);

        return view('articles.index', compact('articles'));
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return Application|Factory|View
     */
    public function show(Article $article): View|Factory|Application
    {
        $nextArticle = $article->where('created_at', '>', $article->created_at)
            ->oldest()
            ->first();

        $prevArticle = $article->where('created_at', '<', $article->created_at)
            ->latest()
            ->first();

        return view('articles.show', compact('article', 'nextArticle', 'prevArticle'));
    }
}
