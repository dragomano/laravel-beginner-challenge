<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !! auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => 'int|required|exists:categories,id',
            'tags' => 'array|nullable',
            'title' => 'string|required|min:3|max:155',
            'content' => 'string|required',
            'image_link' => 'url|nullable',
            'image' => 'image|nullable|max:4096'
        ];
    }
}
